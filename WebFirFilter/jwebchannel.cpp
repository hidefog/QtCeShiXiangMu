﻿#include "jwebchannel.h"
#include <QDebug>

JWebChannel::JWebChannel(QObject *parent) : QObject(parent)
{

}

void JWebChannel::SendMsg(QWebEnginePage* page, const QString &msg)
{
    page->runJavaScript(QString("recvMessage('%1');").arg(msg));
}

void JWebChannel::RecvJSMsg(const QString &msg)
{
    emit RecvMsg(msg);
}
