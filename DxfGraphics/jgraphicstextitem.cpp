﻿#include "jgraphicstextitem.h"
#include <QDebug>

JGraphicsTextItem::JGraphicsTextItem(const QString &text, int x, int y)
    : m_text(text)
    , x(x)
    , y(y)
    , m_color(Qt::red)
{
}

QRectF JGraphicsTextItem::boundingRect() const
{
    return QRectF(0, 0, x, y);
}

QPainterPath JGraphicsTextItem::shape() const
{
    QPainterPath path;
    path.addRect(0, 0, x, y);
    return path;
}

void JGraphicsTextItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget)
    Q_UNUSED(option)
    painter->save();
    painter->setPen(m_color);
//    painter->scale(1, 1); // 默认为 1
    painter->drawText(x, y, m_text);
    painter->restore();
}

void JGraphicsTextItem::setText(const QString &text)
{
    this->m_text = text;
    update();
}

void JGraphicsTextItem::setColor(const QColor &c)
{
    this->m_color = c;
    update();
}

void JGraphicsTextItem::setX(int x)
{
    this->x = x;
    update();
}

void JGraphicsTextItem::setY(int y)
{
    this->y = y;
    update();
}

