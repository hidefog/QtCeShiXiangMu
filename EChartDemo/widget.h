﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QWebEngineView>
#include <QtWebChannel>
#include "jwebchannel.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_8_clicked();

    void on_pushButton_9_clicked();

    void on_pushButton_10_clicked();

    void on_pushButton_11_clicked();

    void on_pushButton_12_clicked();

    void on_pushButton_13_clicked();

    void RecvJSMeg(const QString &msg);

private:
    Ui::Widget *ui;
    QWebEngineView* WebView;
    QWebChannel* WebChannel;
    JWebChannel* ChObj;
    QVBoxLayout* vLayoutMain;
    QHBoxLayout* hLayoutAddr;
};

#endif // WIDGET_H
